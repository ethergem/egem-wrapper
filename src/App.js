import { Route, Routes } from "react-router-dom";

import { isMobile } from "react-device-detect";

import HomeWrap from "./pages/HomeWrap";

import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Navbar />
      <main>
      <Routes>
        <Route path="/" element={<HomeWrap />} />
      </Routes>
      </main>
      <Footer />
    </div>
  );
}

export default App;
