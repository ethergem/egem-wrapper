
/**
 * List of all supported networks
 */

export const SUPPORTED_CHAIN_IDS = {
    EGEM: 1987
}

export const CHAIN_IDS_TO_NAMES = {
    [SUPPORTED_CHAIN_IDS.EGEM]: 'egem',
}