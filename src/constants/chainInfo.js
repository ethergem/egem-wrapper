import { SUPPORTED_CHAIN_IDS } from './chains'
import egemLogoUrl from '../assets/svg/egem_logo.svg'

/**
 * Chain specific contract details
 */
// Modeled on Uniswap's implementation: https://github.com/Uniswap/interface/blob/b501974a763b9c99e69007e8b00d1b21a4e7094b/src/constants/chainInfo.ts
export const CONTRACT_ADDRESS = {
    [SUPPORTED_CHAIN_IDS.EGEM]: "0xE5fca20e55811D461800A853f444FBC6f5B72BEa",
}

export const CHAIN_INFO = {
    [SUPPORTED_CHAIN_IDS.EGEM]: {
        explorer: 'https://blockscout.egem.io/',
        label: 'EGEM',
        logoUrl: egemLogoUrl,
        addNetworkInfo: {
          nativeCurrency: { name: 'EtherGem', symbol: 'WEGEM', decimals: 18 },
        }
    }
}
