// import EGEMLogo from "../assets/svg/eth_logo.svg";
//
// const HomeWrap = () => {
//   return (
//     <div class="container">
//       <div class="row">
//         <div class="col-5">
//           <h1><br></br>
//             <b>Welcome, wrap your ETH easily and safely</b>
//           </h1>
//         </div>
//         <div class="col-6">
//           <img src={EGEMLogo} style={{ width: "40%" }} />
//         </div>
//         <div class="col"></div>
//       </div>
//     </div>
//   );
// };
//
// export default HomeWrap;

import EGEMLogo from "../assets/svg/egem_logo.svg";
import React, { Component, useEffect, useState } from "react";
import Web3 from "web3";
import { ABI } from "../config";
import ErrorModal from "../components/ErrorModal";
import { act } from "react-dom/cjs/react-dom-test-utils.production.min";
import { CHAIN_INFO, CONTRACT_ADDRESS } from "../constants/chainInfo";
import { SUPPORTED_CHAIN_IDS, CHAIN_IDS_TO_NAMES } from "../constants/chains";

//TODO: add ErrorModal
//MetaMask wallet shown/button if connect
//Dropdown for network switch statements

export default function Wrap({ degree, userLocation, basic }) {
  const [apiData, setApiData] = useState([]);
  const [userWethInput, setUserWethInput] = useState(null);
  const [userEthInput, setUserEthInput] = useState(null);
  const [wethContract, setWethContract] = useState(null);
  const [egemBalance, setAvailableEgemBalance] = useState(null);
  const [wegemBalance, setAvailablewegemBalance] = useState(null);
  const [metamaskAddress, setMetamaskAddress] = useState("");
  const [showToast, setShowToast] = useState();
  const [errorMsg, setErrorMsg] = useState();
  const [chainId, setChainId] = useState(null);

  useEffect(() => {

    if (typeof window.ethereum !== 'undefined') {
      console.log('MetaMask is installed!');
    }
    if (window.ethereum) {
      window.ethereum.on('chainChanged', () => {
        window.location.reload()
      })
      window.ethereum.on('accountsChanged', () => {
        window.location.reload()
      })
    }

    const loadBlockchainData = async () => {
      const web3 = new Web3(Web3.givenProvider || "http://localhost:8545");
      const network = await web3.eth.net.getNetworkType();
      await window.ethereum.enable();
      const addressFromMetamask = await web3.eth.getAccounts();
      const chainId = await web3.eth.getChainId()

      setMetamaskAddress(addressFromMetamask[0]);
      console.log(metamaskAddress, "addddddddr");
      setChainId(chainId);
      console.log('Network: ', CHAIN_IDS_TO_NAMES[chainId]);
      console.log('Chain ID: ', chainId);
      console.log('Contract Address: ', CONTRACT_ADDRESS[chainId])

      //Load the smart contract
      const wethContract = new web3.eth.Contract(
        ABI, CONTRACT_ADDRESS[chainId]
      );
      setWethContract(wethContract);

      if (metamaskAddress) {
        let availableWegem = await wethContract.methods
          .balanceOf(metamaskAddress)
          .call();
        setAvailablewegemBalance(web3.utils.fromWei(availableWegem, 'ether'));
        console.log(availableWegem, "avail Wegem:");

        let availableEgem = await web3.eth.getBalance(metamaskAddress);
        console.log(availableEgem, "avail egem:");
        setAvailableEgemBalance(web3.utils.fromWei(availableEgem, 'ether'));

      }

      //Withdraw() (Unwrap eth function)
      //Deposit() (wrap eth function)
      //Balance() 'weth balance connected:'
    };
    loadBlockchainData();
  }, [metamaskAddress]);

  const renderInputBox = () => {
    return (
      <>
        <div style={{ marginBottom: 150, marginTop: 50 }}>
          <input
            type="number"
            class="form-control"
            min="0"
            placeholder="Enter EGEM amount"
            data-name="eth"
            value={userEthInput}
            onChange={(e) => setUserEthInput(e.target.value)}
            style={{ width: "50%", float: "left" }}
          ></input>
          <button
            onClick={() => onWrapClick()}
            type="button"
            class="btn btn-light"
            style={{ float: "left" }}
          >
            Wrap
          </button>
        </div>
        <input
          type="number"
          class="form-control"
          min="0"
          placeholder="Enter WEGEM amount"
          value={userWethInput}
          data-name="weth"
          onChange={(e) => setUserWethInput(e.target.value)}
          style={{ width: "50%", float: "left" }}
        ></input>
        <button
          type="button"
          class="btn btn-light"
          onClick={() => onUnwrapClick()}
          style={{ float: "left" }}
        >
          Unwrap
        </button>
      </>
    );
  };

  const onWrapClick = () => {
    console.log('wrapping...', userEthInput, 'EGEM')
    return onActivityClick('wrap')
  };

  const onUnwrapClick = () => {
    console.log('unwrapping...', userWethInput, 'WEGEM')
    onActivityClick('unwrap')
  };

  const onActivityClick = (activity) => {
    try {
      if (metamaskAddress) {
        let web3js = new Web3(window.ethereum);
        let input = activity == 'wrap' ? userEthInput : userWethInput
        let userInputInWei = web3js.utils.toWei(input, 'ether');
        let params = setParams(activity, userInputInWei);
        if (userInputInWei >= 1) {
          web3js.eth.sendTransaction(params);
        }
      }
    } catch (err) {
      setShowToast(true);
      setErrorMsg(
        "Something went wrong, please try again or report this issue"
      );
      console.log(err, "2Generic Error TODO: Popup, ");
    }
  };

  const setParams = (activity, userInputInWei) => {
    let params = {
      'to': CONTRACT_ADDRESS[chainId],
      'from': metamaskAddress,
    }
    switch (activity) {
      case 'wrap': {
        params['data'] = wethContract.methods.deposit().encodeABI();
        params['value'] = userInputInWei
      } break;
      case 'unwrap': {
        params['data'] = wethContract.methods.withdraw(userInputInWei).encodeABI()
      } break;
    }
    console.log(params)
    return params
  };

  return (
    <div class="container">
      <div class="row">
        <div class="col-5">
          <h1>
            <br></br>
            <b>Balances:</b>
          </h1>
          <div><a href={ 'https://blockscout.egem.io/address/' + metamaskAddress } target="_Blank">{metamaskAddress}</a></div>
          <br></br>
          <div>EGEM available: {egemBalance}</div>
          <div>Wrapped EGEM available: {wegemBalance}</div>
          {renderInputBox()}
        </div>
        <div class="col-6">
          <img src={EGEMLogo} style={{ width: "40%" }} />
        </div>
        <div class="col"></div>
      </div>
      {showToast ? (
        <ErrorModal
          showToastFromProp={showToast}
          errorMsg={errorMsg}
        ></ErrorModal>
      ) : null}
    </div>
  );
}
