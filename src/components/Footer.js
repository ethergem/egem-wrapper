import { NavLink } from "react-router-dom";
import { faGift } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';
import EGEMLogo from "../assets/svg/egem_logo.svg";



import "./Footer.css";

const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div class="link-boxes">
          <ul class="box">
            <li class="link_name">Wrap EGEM</li>
            <li>
              <img width="20%" src={EGEMLogo} />
            </li>
          </ul>
          <ul class="box">
            <li class="link_name">Project</li>
            <li>
              {" "}
              <a
                href="https://egem.io"
                target="_blank"
              >
                Website
              </a>
            </li>
            <li>
              {" "}
              <a
                href="https://bridge.egem.io"
                target="_blank"
              >
                Bridge
              </a>
            </li>
          </ul>
          <ul class="box">
            <li class="link_name">Support</li>
            <li>
              <a href="https://discord.egem.io/" target="_blank">
                Discord
              </a>
            </li>

          </ul>
          <ul class="box">
            <li class="link_name">Social</li>
            <li>
              <a
                target="_blank"
                href="https://gitlab.com/ethergem/egem-wrapper"
              >
                Gitlab
              </a>
            </li>
            <li>
              <a href="#"></a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
