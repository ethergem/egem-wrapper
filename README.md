# About This Repository

Wrap and unwrap your EGEM with this simple React frontend dapp that connects to your metamask and uses the WEGEM contract

# How To Run This Project On Your Local Machine
1. In root folder: Install all dependencies with `npm install`
2. In /egem-wrapper/src: Start the development server server via `npm start`.
